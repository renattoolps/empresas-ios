//
//  Data+Extension.swift
//  Data
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation

public extension Data {
    
    // MARK: - Public Methods
    func convertToModel<T: Decodable>() -> T? {
        return try? JSONDecoder().decode(T.self, from: self)
    }
    
    func convertToJson() -> [String: Any]? {
        return try? JSONSerialization.jsonObject(with: self, options: .allowFragments) as? [String: Any]
    }
}
