//
//  HttpClientPost.swift
//  Data
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation

public protocol HttpPostClient {
    typealias HttpPostResponse = (body: Data?, headers: [String: Any]?)
    func post(to url: URL, with data: Data?, completion: @escaping (Result<HttpPostResponse, HttpClientError>) -> Void)
}
