//
//  RemoteLogin.swift
//  Data
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation
import Domain

public class RemoteLogin: LoginUseCase {
    // MARK: - Private Properties
    private let httpPostClient: HttpPostClient
    private let url: URL
    
    // MARK: Inits
    public init(httpPost: HttpPostClient, url: URL) {
        self.httpPostClient = httpPost
        self.url = url
    }
    
    // MARK: - Public Methods
    public func auth(withAccount: LoginModel.Request, completion: @escaping (Result<AccountModel, Error>) -> Void) {
        
        guard let data: Data = withAccount.convertToData() else {
            return completion(.failure(DomainError.falureConvertionToData))
        }
        
        httpPostClient.post(to: self.url, with: data) { result in
            switch result {
                
            case .success(let response):
                guard let response: LoginModel.Response = response.body?.convertToModel() else {
                    return completion(.failure(DomainError.failureConvertionToModel))
                }
                
                let model: AccountModel = AccountModel(email: response.investor.email,
                                                       name: response.investor.investor_name)
                completion(.success(model))
                
            case .failure(let error):
                completion(.failure(RemoteLoginError.requestAuthentication(httpError: error)))
            }
        }
    }
}

public enum RemoteLoginError: Error, LocalizedError {
    case requestAuthentication (httpError: HttpClientError)
    
    public var errorDescription: String? {
        switch self {
        case .requestAuthentication(let httpError):
            switch httpError {
            case .noConnectivity:
                return "Verifique sua conexão com a internet."
            case .serverError:
                return "Houve um erro ao se comunicar com o servidor. Tente novamente"
            case .unauthorized:
                return "Credenciais Incorretas."
            default:
                return "Falha ao autenticar. Tente novamente."
            }
        }
    }
}
