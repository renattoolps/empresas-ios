//
//  Account.swift
//  Domain
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation

public class AccountModel {
    // MARK: - Public Properties
    let email: String
    let name: String
    
    // MARK: - Inits
    public init(email: String, name: String) {
        self.email = email
        self.name = name
    }
}
