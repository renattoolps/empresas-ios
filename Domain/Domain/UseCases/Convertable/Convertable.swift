//
//  Model.swift
//  Domain
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation

public protocol Convertable: Codable { }

public extension Convertable {
    
    // MARK: - Public Methods
    func convertToData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
    
    func convertToJson() -> [String: Any]? {
        guard let data: Data = convertToData() else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
    }
}
