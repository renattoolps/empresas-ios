//
//  LoginUseCase.swift
//  Domain
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation

public protocol LoginUseCase {
    func auth(withAccount: LoginModel.Request, completion: @escaping ( Result<AccountModel, Error>) -> Void)
}

public struct LoginModel {
    
    public struct Request: Encodable, Convertable {
        // MARK: - Public Properties
        public let email: String
        public let password: String
        
        // MARK: - Inits
        public init(email: String, password: String) {
            self.email = email
            self.password = password
        }
    }
    
    public struct Response: Decodable {
        // MARK: - Public Properties
        public let investor: Investor
        public let success: Bool
        
        public struct Investor: Decodable {
            public let id: Int
            public let investor_name: String
            public let email: String
            public let city: String
            public let country: String
            public let balance: Double
            public let first_access: Bool
            public let super_angel: Bool
            
        }
    }
}
