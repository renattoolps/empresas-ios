//
//  AlamofireAdapter.swift
//  Infra
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation
import Alamofire
import Data

public final class AlamofireAdapter: HttpPostClient {
    
    // MARK: - Private Properties
    private let session: Session
    
    // MARK: - Inits
    public init(session: Session = .default) {
        self.session = session
    }
    
    // MARK: - Public Methods
    public func post(to url: URL, with data: Data?, completion: @escaping (Result<HttpPostResponse, HttpClientError>) -> Void) {
        var json: [String: Any]?
        
        defer {            
            session.request(url, method: .post, parameters: json,encoding: JSONEncoding.default).responseData { (response) in
                guard let statusCode: Int = response.response?.statusCode else { return completion(.failure(.noConnectivity))}
                switch response.result {
                case .failure: completion(.failure(.noConnectivity))
                case .success(let data):
                    switch statusCode {
                    case 204:
                        completion(.success((nil, nil)))
                    case 200...299:
                        completion(.success((data, nil)))
                    case 401:
                        completion(.failure(.unauthorized))
                    case 403:
                        completion(.failure(.forbidden))
                    case 400...499:
                        completion(.failure(.badRequest))
                    case 500...599:
                        completion(.failure(.serverError))
                    default:
                        completion(.failure(.noConnectivity))
                    }
                }
            }.resume()
        }
        
        guard let data: Data = data else { return }
        json = data.convertToJson()
    }
}
