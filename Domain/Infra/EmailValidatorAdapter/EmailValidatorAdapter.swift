//
//  EmailValidatorAdapter.swift
//  Infra
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation
import Validation

public class EmailValidatorAdapter: EmailValidationProtocol {
    // MARK: - Private Properties
    private let pattern: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    // MARK: - Inits
    public init() { }

    // MARK: - Public Methods
    public func validate(_ email: String) -> Bool {
        let range: NSRange = NSRange(location: 0, length: email.utf8.count)
        let regex: NSRegularExpression?  = try? NSRegularExpression(pattern: pattern)
        return regex?.firstMatch(in: email, range: range) != nil
    }
    
}
