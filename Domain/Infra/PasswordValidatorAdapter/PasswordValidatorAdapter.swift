//
//  PasswordValidatorAdapter.swift
//  Infra
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation
import Validation

public class PasswordValidatorAdapter: PasswordValidationProtocol {
    // MARK: - Private Properties
    private let minLimit: Int = 3
    private let maxLimit: Int = 100
    
    // MARK: - Inits
    public init() { }
    
    // MARK: - Public Methods
    public func validate(_ password: String) -> Bool {
        return password.count >= minLimit && password.count <= maxLimit
    }
}
