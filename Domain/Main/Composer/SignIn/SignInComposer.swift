//
//  SignInComposer.swift
//  Main
//
//  Created by Renato Lopes on 04/12/20.
//
import UIKit
import Domain
import Data
import Infra
import Presentation
import UI
import Validation

public class SignInComposer {
    static func initialize() -> SignInViewController {
        let postClient: HttpPostClient = AlamofireAdapter()
        // TODO: - Remove forced in url

        let url: URL = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in")!
        let useCase: LoginUseCase = RemoteLogin(httpPost: postClient, url: url)
        let controller: SignInViewController = SignInViewController()
        
        let presenter: SignInPresenter = SignInPresenter(loading: controller, alert: controller, login: useCase, validation: createValidations())
        controller.signIn = presenter.authentication
        return controller
    }
    
    private static func createValidations() -> ValidationProtocol {
        // Adapters
        let emailAdapter: EmailValidatorAdapter = EmailValidatorAdapter()
        let passwordAdapter: PasswordValidatorAdapter = PasswordValidatorAdapter()
        
        // Validations
        let emailValidation: ValidationProtocol = EmailValidator(fieldName: "email", fieldLabel: "Email", emailValidation: emailAdapter)
        let passwordValidaton: ValidationProtocol = PasswordValidator(fieldName: "password", fieldLabel: "Senha", passwordValidation: passwordAdapter)
        let requiredEmailField: ValidationProtocol = RequiredFieldValidation(fieldName: "email", fieldLabel: "Email")
        
        let requiredPasswordField: ValidationProtocol = RequiredFieldValidation(fieldName: "password", fieldLabel: "Senha")
        
        // Composition
        let compositeValidation = ValidationComposite(validations: [
            emailValidation,
            passwordValidaton,
            requiredEmailField,
            requiredPasswordField
        ])
        
        return compositeValidation
    }
}
