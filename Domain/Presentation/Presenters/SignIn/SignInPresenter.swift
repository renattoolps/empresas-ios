//
//  SignInPresenter.swift
//  Presentation
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation
import Domain
import Validation

public class SignInPresenter  {
    // MARK: - Private Properties
    private let loadingView: LoadingView
    private let alertView: AlertView
    private let login: LoginUseCase
    private let validation: ValidationProtocol
    
    // MARK: - Inits
    public init(loading: LoadingView, alert: AlertView, login: LoginUseCase, validation: ValidationProtocol) {
        self.alertView = alert
        self.loadingView = loading
        self.login = login
        self.validation = validation
    }
    
    // MARK: - Public Methods
    public func authentication(with accountViewModel: AccountViewModelRequest, completion: @escaping () -> Void) {
        let (request, error): (LoginModel.Request?, String?) = makeRequestModel(accountViewModel)
        
        guard let accountRequest: LoginModel.Request = request else {
            return alertView.showMessage(viewModel: AlertViewModel(title: "Verificar Campos", message: error ?? ""))
        }
        
        loadingView.display(viewModel: LoadingViewModel(isLoading: true))
        login.auth(withAccount: accountRequest) { result in
            switch result {

            case .success(_):
                self.loadingView.display(viewModel: LoadingViewModel(isLoading: false))
                completion()
            case .failure(let error):
                self.loadingView.display(viewModel: LoadingViewModel(isLoading: false))
                self.alertView.showMessage(viewModel: AlertViewModel(title: "Erro ao autenticar", message: error.localizedDescription))
            }
        }

    }
    
    // MARK: - Private Methods
    private func makeRequestModel(_ viewModel: AccountViewModelRequest) -> (request: LoginModel.Request?, error: String?) {
        
        guard let email: String = viewModel.email, let password: String = viewModel.password else {
            return (nil, validation.validate(data: nil))
        }
        let request: LoginModel.Request = LoginModel.Request(email: email, password: password)
        
        guard let errorMessage: String = validation.validate(data: request.convertToJson()) else {
            return (request, nil)
        }
        // If exist any incorrect field, when send error
        return (nil, errorMessage)
        
    }
}
