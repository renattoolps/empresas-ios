//
//  AccountViewModelRequest.swift
//  Presentation
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation

public struct AccountViewModelRequest {
    // MARK: - Public Properties
    public let email: String?
    public let password: String?
    
    // MARK: - Inits
    public init(email: String? = nil, password: String? = nil) {
        self.email = email
        self.password = password
    }
}
