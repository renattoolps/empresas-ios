//
//  CurvedView.swift
//  UI
//
//  Created by Renato Lopes on 04/12/20.
//

import UIKit

class HeaderCurvedView:UIView {
    // MARK: - Public Properties
    public var continueUpdating: Bool = true
    
    // MARK: - Public Methods
    override func layoutSubviews() {
        super.layoutSubviews()
        guard continueUpdating else { return }
        
        // Setup to Draw
        let path: UIBezierPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        
        path.move(to: CGPoint(x: 0, y: frame.height))
        path.addQuadCurve(to: CGPoint(x: frame.width, y: frame.height),
        controlPoint: CGPoint(x: frame.width / 2 , y: frame.height + 60 ))
        
        path.close()
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        
        // Add draw in shape layer
        let gradient = CAGradientLayer()
        gradient.defaultBackground()
        
        layer.insertSublayer(gradient, at:0)
        gradient.mask = shapeLayer
        
        // Stop layoutSubview updates
        continueUpdating = false
    }
}
