//
//  LoadingAnimationView.swift
//  UI
//
//  Created by Renato Lopes on 04/12/20.
//

import UIKit
import Stevia

public class LoadingViewAnimation {
    var backgroundView: UIView = UIView()
    
    init() { /* no-op */}
    
    /// Method show loading view
    ///     - Parameters:
    ///         - inView: view where the loading should appear
    public func startLoading(inView view: UIView) {
        self.stopLoading()
        let backgroundAlpha: CGFloat = 0.5
        let backgroundViewColor: UIColor = UIColor.black.withAlphaComponent(backgroundAlpha)
        
        backgroundView.frame = view.frame
        backgroundView.backgroundColor = backgroundViewColor
        
        let firstImage = UIImageView(image: UIImage.image(withName: "loading_first"))
        let secondImage = UIImageView(image: UIImage.image(withName: "loading_second"))
        
        view.subviews(backgroundView)
        backgroundView.leading(0).trailing(0).top(0).bottom(0)
        
        backgroundView.subviews(firstImage, secondImage)
        firstImage.leading(0).trailing(0).top(0).bottom(0)
        secondImage.leading(0).trailing(0).top(0).bottom(0)
        firstImage.contentMode = .center
        secondImage.contentMode = .center

        
        firstImage.rotate()
        firstImage.startAnimating()
        secondImage.rotate()
        secondImage.startAnimating()
    }

    /// Method stop and remove loading
    public func stopLoading() {
        backgroundView.removeFromSuperview()
    }
}
