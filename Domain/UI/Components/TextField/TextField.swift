//
//  TextField.swift
//  UI
//
//  Created by Renato Lopes on 02/12/20.
//

import UIKit
import Stevia

public class TextField: UIView {
    enum TextFieldState {
        case normal (infor: String?)
        case error (message: String?)
    }
    
    // MARK: - UI Components
    private let messageLabel: UILabel = UILabel()
    private let textField: UITextField = UITextField()

    // MARK: - Public Properties
    var currentState: TextFieldState {
        didSet {
            updateFieldStyle()
        }
    }
    
    var text: String?  {
        return textField.text
    }
    
    // MARK: - Inits
    init() {
        self.currentState = .normal(infor: nil)
        super.init(frame: .zero)
        setupSubviews()
        setupAutolayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    private func updateFieldStyle() {
        switch currentState {
        case .normal(let infor):
            applyNormalStyle()
            messageLabel.text = infor
            
        case .error(let errorMessage):
            applyErrorStyle()
            messageLabel.text = errorMessage
        }
    }
    
    public func setupMessageLabel(font: UIFont, color: UIColor) {
        messageLabel.style { (label) in
            label.font = font
            label.textAlignment = .right
            label.textColor = color
            label.numberOfLines = 1
        }
    }
    
    public func setupTextField(font: UIFont, type: UIKeyboardType = .default, isSecurityField: Bool = false) {
        textField.style { (field) in
            field.addBorder(with: 4)
            field.font = font
            field.keyboardType = type
            field.backgroundColor = StyleGuide.Color.Field.default
            currentState = .normal(infor: nil)
            guard isSecurityField else { return }
            field.isSecureTextEntry = isSecurityField
            addStyleToPasswordField()
        }
    }
    
    private func setupSubviews() {
        subviews(textField, messageLabel)
    }
    
    private func setupAutolayout() {
        textField.top(0).leading(0).trailing(0).height(48)
        messageLabel.leading(0).trailing(0).Top == textField.Bottom + 2
    }
}

// MARK: - Styles
extension TextField {
    private func applyNormalStyle() {
        textField.style { (field) in
            field.layer.borderColor = UIColor.clear.cgColor
            field.layer.borderWidth = 0
            field.textColor =  StyleGuide.Color.Label.default
            field.tintColor = StyleGuide.Color.Field.cursor
        }
    }
    
    private func applyErrorStyle() {
        textField.style { (field) in
            field.layer.borderColor = StyleGuide.Color.Field.error.cgColor
            field.layer.borderWidth = 1
            field.textColor = StyleGuide.Color.Label.default
            field.tintColor = StyleGuide.Color.Field.cursor
        }
    }
}


// MARK: Password Field
extension TextField {
    
    private func addStyleToPasswordField() {
        let button: UIButton = UIButton()

        textField.rightViewMode = .always
        button.setImage(UIImage.image(withName: "password_visibility"), for: .normal)
        
        button.addTarget(self, action: #selector(changeVisibilityField), for: .touchUpInside)
        
        textField.rightView = button
        button.imageEdgeInsets = UIEdgeInsets(top: .zero, left: -10, bottom: .zero, right: 5)
    }
    
    @objc
    private func changeVisibilityField() {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
    }
}
