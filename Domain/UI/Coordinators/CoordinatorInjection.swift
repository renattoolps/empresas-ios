//
//  CoordinatorInjection.swift
//  UI
//
//  Created by Renato Lopes on 07/12/20.
//

import Foundation
import UIKit

// TODO: Change the manager for generic class witrh possibility of add any dependency type.
public final class ViewControllersInjectorManager {
    // MARK: - Private Properties
    private static var factories: [String : () -> UIViewController] = [:]
    
    // MARK: - Public Methods
    public static func register(withKey: String, factory: @escaping () -> UIViewController) {
        factories[withKey] = factory
    }
    
    public static func get(factory: String) -> (() -> UIViewController)? {
        return factories[factory]
    }
}
