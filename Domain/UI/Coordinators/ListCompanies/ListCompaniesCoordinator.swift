//
//  ListCompaniesCoordinator.swift
//  UI
//
//  Created by Renato Lopes on 11/12/20.
//

import UIKit

public final class ListCompaniesCoordinator: NavigationCoordinator {
    var navigationController: UINavigationController?
    var childCoordinators: [Coordinator]
    
    public init(navigation: UINavigationController) {
        navigationController = navigation
        childCoordinators = [Coordinator]()
    }
    
    func start() {
        let factory = ViewControllersInjectorManager.get(factory: ListCompaniesViewController.description())
        guard let controller: ListCompaniesViewController = factory?() as? ListCompaniesViewController else { return }
        navigationController?.setViewControllers([controller], animated: true)
    }
}
