//
//  MainCoordinator.swift
//  UI
//
//  Created by Renato Lopes on 11/12/20.
//

import UIKit

public final class MainCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = [Coordinator]()
    var rootWindow: UIWindow? = nil
    
    public init(rootWindow: UIWindow) {
        self.rootWindow = rootWindow
    }
    
    public func start() {
        let signInCoordinator: SignInCoordinator = SignInCoordinator(window: rootWindow!)
        childCoordinators.append(signInCoordinator)
        signInCoordinator.start()
        rootWindow?.makeKeyAndVisible()
    }
}
