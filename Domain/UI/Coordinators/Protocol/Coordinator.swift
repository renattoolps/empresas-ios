//
//  Coordinator.swift
//  UI
//
//  Created by Renato Lopes on 06/12/20.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    
    func start()
}

protocol NavigationCoordinator: Coordinator {
    var navigationController: UINavigationController? { get set }
}

protocol TabBarCoordinator: Coordinator {
    var tabBarController: UITabBarController? { get set }
}
