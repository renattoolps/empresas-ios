//
//  SignInCoordinator.swift
//  UI
//
//  Created by Renato Lopes on 06/12/20.
//

import UIKit

public final class SignInCoordinator: NavigationCoordinator {
    var navigationController: UINavigationController?
    var childCoordinators: [Coordinator]
    var rootWindow: UIWindow?
    
    public init(window: UIWindow) {
        self.childCoordinators = [Coordinator]()
        self.rootWindow = window
    }
    
    func start() {
        let factory = ViewControllersInjectorManager.get(factory: SignInViewController.description())
        guard let controller: SignInViewController = factory?() as? SignInViewController else { return }
        controller.openListCompaniesView = openHomeView
        rootWindow?.rootViewController = controller
    }
    
    public func openHomeView() {
        let navigation: UINavigationController = UINavigationController()
        let homeCoordinator: NavigationCoordinator = ListCompaniesCoordinator(navigation: navigation)
        rootWindow?.rootViewController = navigation
        childCoordinators.append(homeCoordinator)
        homeCoordinator.start()
    }
}



