//
//  CAGradientLayer+Extension.swift
//  UI
//
//  Created by Renato Lopes on 04/12/20.
//

import UIKit

extension CAGradientLayer {
    
    func defaultBackground() {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        self.colors = StyleGuide.Color.Background.gradient.compactMap({ $0?.cgColor })
        self.startPoint = CGPoint(x: 1.0, y: 0.0)
        self.endPoint = CGPoint(x: 0.0, y: 1.0)
    }
}
