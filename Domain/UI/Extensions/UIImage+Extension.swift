//
//  UIImage+Extension.swift
//  UI
//
//  Created by Renato Lopes on 04/12/20.
//

import UIKit

extension UIImage {
    private class ImageReferenceClass {}
    
    static func image(withName name: String) -> UIImage? {
        return UIImage(named: name, in: Bundle(for: ImageReferenceClass.self), compatibleWith: nil)
    }
}
