//
//  UIViewController+Extension.swift
//  UI
//
//  Created by Renato Lopes on 04/12/20.
//

import UIKit

extension UIViewController {
    
    /// This method add event to close the keyboard when view is touched
    func closeKeyboardWhenTouchedInView() {
        let keyName: String = "\(#file)_\(#function)_close_keyboard"
        if let removeGesture: UIGestureRecognizer = view.gestureRecognizers?.first(where: { $0.name == keyName }) {
            view.removeGestureRecognizer(removeGesture)
        }
        
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
        gesture.name = keyName
        view.addGestureRecognizer(gesture)
    }
    
    @objc
    public func closeKeyboard() {
        view.endEditing(true)
    }
}
