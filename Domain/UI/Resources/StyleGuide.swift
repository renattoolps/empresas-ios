//
//  StyleGuide.swift
//  UI
//
//  Created by Renato Lopes on 30/11/20.
//

import UIKit

struct StyleGuide {
    struct Color {
        struct Label {
            static let `default`: UIColor = UIColor(hex: "#666666") ?? .gray
            static let error: UIColor = UIColor(hex: "#EB5757") ?? .red
        }
        
        struct Field {
            static let `default`: UIColor = UIColor(hex: "#E5E5E5") ?? .gray
            static let cursor: UIColor = UIColor(hex: "#E01E69") ?? .black
            static let error: UIColor = UIColor(hex: "#EB5757") ?? .red
        }
        
        struct Button {
            static let `default`: UIColor = UIColor(hex: "#E01E69") ?? .red
        }
        
        struct Background {
            static let `default`: UIColor = UIColor(hex: "#F5F5F5") ?? .red
            
            static let gradient: [UIColor?] = [
                UIColor(hex: "#8E24B8"),
                UIColor(hex: "#A61FA5"),
                UIColor(hex: "#B21C93"),
                UIColor(hex: "#B21C87"),
                UIColor(hex: "#BC407E"),
                UIColor(hex: "#E17EB3")
            ]
        }
    }
    
    struct Font {
        static let `default`: UIFont = UIFont(name: "Rubik", size: 14) ?? UIFont.systemFont(ofSize: 14)
        static let buttonTitle: UIFont = UIFont(name: "Rubik", size: 16) ?? UIFont.systemFont(ofSize: 16)
        static let textFieldTitle: UIFont = UIFont(name: "Rubik", size: 16) ?? UIFont.systemFont(ofSize: 16)
        static let headerTitle: UIFont = UIFont(name: "Rubik", size: 20) ?? UIFont.systemFont(ofSize: 20)

    }
}

extension UIColor {
    
    convenience init?(hex: String, alpha: CGFloat = 1) {
        var chars = Array(hex.hasPrefix("#") ? hex.dropFirst() : hex[...])
        switch chars.count {
        case 3: chars = chars.flatMap { [$0, $0] }
        case 6: break
        default: return nil
        }
        self.init(red: .init(strtoul(String(chars[0...1]), nil, 16)) / 255,
                green: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
                 blue: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
                alpha: alpha)
    }
    
}
