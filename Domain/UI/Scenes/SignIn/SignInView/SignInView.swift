//
//  SignInView.swift
//  UI
//
//  Created by Renato Lopes on 30/11/20.
//

import Foundation
import UIKit
import Stevia

public class SignInView: UIView {
    // MARK: - UI Elements
    private var containerView: UIView = UIView()
    private var headerView: WellcomeHeaderVew = WellcomeHeaderVew()
    private var emailLabel: UILabel = UILabel()
    private var passwordLabel: UILabel = UILabel()
    private var emailField: TextField = TextField()
    private var passwordField: TextField = TextField()
    private var confirmButton: UIButton = UIButton()
    
    // MARK: - Delegate
    private weak var delegate: SignInViewReactivityEventsProtocol?
    
    // MARK: - Inits
    init(delegate: SignInViewReactivityEventsProtocol? = nil) {
        super.init(frame: .zero)
        self.delegate = delegate
        setupStyle()
        setupSubviews()
        setupAutolayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - Public Methods
    public func showEmailError(message: String) {
        emailField.currentState = .error(message: message)
        let font: UIFont = StyleGuide.Font.default
        let error: UIColor = StyleGuide.Color.Label.error
        emailField.setupMessageLabel(font: font, color: error)
    }
    
    public func showPasswordError(message: String) {
        passwordField.currentState = .error(message: message)
        let font: UIFont = StyleGuide.Font.default
        let error: UIColor = StyleGuide.Color.Label.error
        passwordField.setupMessageLabel(font: font, color: error)
    }
    
    public func resetFields() {
        emailField.currentState = .normal(infor: nil)
        passwordField.currentState = .normal(infor: nil)
    }
    
    // MARK: - Private Methods
    private func setupStyle() {
        backgroundColor = StyleGuide.Color.Background.default
        setupContainerView()
    }
    
    private func setupContainerView() {
        containerView.backgroundColor = StyleGuide.Color.Background.default

        setupLabels()
        setupFields()
        setupButtons()
    }
    
    private func setupLabels() {
        emailLabel.style { (label) in
            label.font = StyleGuide.Font.default
            label.tintColor = StyleGuide.Color.Label.default
            label.text = "Email"
        }
        
        passwordLabel.style { (label) in
            label.font = StyleGuide.Font.default
            label.tintColor = StyleGuide.Color.Label.default
            label.text = "Senha"
        }
    }
    
    private func setupFields() {
        emailField.setupTextField(font: StyleGuide.Font.textFieldTitle, type: .emailAddress)
        passwordField.setupTextField(font: StyleGuide.Font.textFieldTitle, type: .default, isSecurityField: true)
    }
    

    
    private func setupButtons() {
        confirmButton.style { (button) in
            button.addBorder(with: 8)
            button.backgroundColor = StyleGuide.Color.Button.default
            button.setTitle("Entrar", for: .normal)
            button.titleLabel?.font = StyleGuide.Font.buttonTitle
            button.addTarget(self, action: #selector(didTouchedInSignInButton), for: .touchUpInside)
        }
    }
    
    private func setupSubviews() {
        subviews(headerView, containerView)
        containerView.subviews(emailLabel, emailField, passwordLabel, passwordField, confirmButton)
    }
    
    private func setupAutolayout() {
        // Header
        headerView.leading(0).trailing(0).top(0)
        headerView.height(260)
        
        // Container
        containerView.leading(0).trailing(0).bottom(0).Top == headerView.Bottom
        
        // Email
        emailLabel.leading(20).trailing(24).top(6)
        emailField.leading(20).trailing(24).Top == emailLabel.Bottom + 4
        emailField.height(60)
        
        // Password
        passwordLabel.leading(20).trailing(24).Top == emailField.Bottom + 16
        passwordField.leading(20).trailing(24).Top == passwordLabel.Bottom + 4
        passwordField.height(60)
        
        // Confirm Button
        confirmButton.leading(31).trailing(28).Top == passwordField.Bottom + 54
        confirmButton.height(48)
    }
    
    // MARK: - Delegate Actions
    @objc
    private func didTouchedInSignInButton() {
        let email: String? = emailField.text
        let password: String? = passwordField.text
        delegate?.didTouchedInSignIn(email, password: password)
    }
}
