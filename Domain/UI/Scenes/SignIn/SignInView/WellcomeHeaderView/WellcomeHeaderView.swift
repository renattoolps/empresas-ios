//
//  WellcomeHeaderView.swift
//  UI
//
//  Created by Renato Lopes on 04/12/20.
//

import UIKit
import Stevia

class WellcomeHeaderVew: UIView {
    // MARK: - Private Properties
    private let headerCurved: HeaderCurvedView = HeaderCurvedView()
    private let iconImageView: UIImageView = UIImageView()
    private let titleLabel: UILabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        style()
        setupSubviews()
        setupAutolayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    private func style() {
        backgroundColor = StyleGuide.Color.Background.default
        iconImageView.image = UIImage.image(withName: "logo_home_without_name")
        
        titleLabel.style { (label) in
            label.text = "Seja bem vindo ao empresas!"
            label.tintColor = .white
            label.textColor = .white
            label.font = StyleGuide.Font.headerTitle
            label.textAlignment = .center
        }

    }
    
    private func setupSubviews() {
        subviews(headerCurved.subviews(iconImageView, titleLabel))
    }
    
    private func setupAutolayout() {
        headerCurved.leading(0).trailing(0).top(0).height(200)
        
        iconImageView.centerHorizontally().Top == safeAreaLayoutGuide.Top + 20
        titleLabel.leading(0).trailing(0).Top == iconImageView.Bottom + 30
    }
}
