//
//  SignInViewController.swift
//  UI
//
//  Created by Renato Lopes on 01/12/20.
//

import UIKit
import Presentation

/// Responsability for send events to request login of account sended by parameters
protocol SignInViewEventsProtocol {
    var signIn: ((AccountViewModelRequest, @escaping () -> Void) -> Void)? { get set }
}

/// Responsabililty for detect when any event is trigged in SignInView
protocol SignInViewReactivityEventsProtocol: AnyObject {
    func didTouchedInSignIn(_ email: String?, password: String?)
}

public class SignInViewController: UIViewController, SignInViewEventsProtocol {
    // MARK: - Events
    public var signIn: ((AccountViewModelRequest, @escaping () -> Void) -> Void)?
    public var openListCompaniesView: (() -> Void)? = nil
    
    // MARK: - Private Properties
    private var customView: SignInView?
    private let loading = LoadingViewAnimation()
    
    // MARK: - Lifecycle
    public override func loadView() {
        customView = SignInView(delegate: self)
        view = customView
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        closeKeyboardWhenTouchedInView()
    }
}

extension SignInViewController: SignInViewReactivityEventsProtocol {
    func didTouchedInSignIn(_ email: String?, password: String?) {
        signIn?(AccountViewModelRequest(email: email, password: password), {
            print("Success authentication!")
            self.openListCompaniesView?()
        })
    }
}

extension SignInViewController: LoadingView {
    public func display(viewModel: LoadingViewModel) {
        closeKeyboard()
        if viewModel.isLoading {
            loading.startLoading(inView: view)
        } else {
            loading.stopLoading()
        }
    }
}

extension SignInViewController: AlertView {
    public func showMessage(viewModel: AlertViewModel) {
        customView?.showEmailError(message: "")
        customView?.showPasswordError(message: viewModel.message)
    }
}
