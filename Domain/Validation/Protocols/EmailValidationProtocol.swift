//
//  EmailValidationProtocol.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

/// This protocol is user to validation of email field
public protocol EmailValidationProtocol {
    func validate(_ email: String) -> Bool
}
