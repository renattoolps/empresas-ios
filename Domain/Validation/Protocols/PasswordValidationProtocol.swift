//
//  PasswordValidationProtocol.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

/// This protocol is user to validation of password field
public protocol PasswordValidationProtocol {
    func validate(_ password: String) -> Bool
}
