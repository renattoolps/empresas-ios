//
//  ValidationProtocol.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

public protocol ValidationProtocol {
    func validate(data: [String : Any]?) -> String?
}
