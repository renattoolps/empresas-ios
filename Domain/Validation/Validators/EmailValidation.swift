//
//  EmailValidation.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

public class EmailValidator: ValidationProtocol {
    // MARK: - Private Properties
    private let fieldName: String
    private let fieldLabel: String
    private let emailValidation: EmailValidationProtocol
    
    // MARK: - Inits
    public init(fieldName: String, fieldLabel: String, emailValidation: EmailValidationProtocol) {
        self.fieldName = fieldName
        self.fieldLabel = fieldLabel
        self.emailValidation = emailValidation
    }
    
    // MARK: - Public Methods
    public func validate(data: [String : Any]?) -> String? {
        guard let field: String = data?[fieldName] as? String, emailValidation.validate(field) else {
            return "O campo \(fieldLabel) é inválido."
        }

        return nil
    }
}
