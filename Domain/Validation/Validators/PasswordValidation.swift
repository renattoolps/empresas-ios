//
//  PasswordValidation.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

public class PasswordValidator: ValidationProtocol {
    
    // MARK: - Private Properties
    private let fieldName: String
    private let fieldLabel: String
    private let passwordValidation: PasswordValidationProtocol

    // MARK: - Inits
    public init(fieldName: String, fieldLabel: String, passwordValidation: PasswordValidationProtocol) {
        self.fieldName = fieldName
        self.fieldLabel = fieldLabel
        self.passwordValidation = passwordValidation
    }
    
    // MARK: - Public Methods
    public func validate(data: [String : Any]?) -> String? {
        guard let field: String = data?[fieldName] as? String, passwordValidation.validate(field) else {
            return "O campo \(fieldLabel) é inválido."
        }
        return nil
    }
}
