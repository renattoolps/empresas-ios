//
//  RequiredFieldsValidation.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

public class RequiredFieldValidation: ValidationProtocol {
    // MARK: - Private Properties
    private let fieldName: String
    private let fieldLabel: String
    
    // MARK: - Inits
    public init(fieldName: String, fieldLabel: String) {
        self.fieldName = fieldName
        self.fieldLabel = fieldLabel
    }
    
    public func validate(data: [String : Any]?) -> String? {
        guard let field: String = data?[fieldName] as? String, !field.isEmpty else {
            return "O campo \(fieldLabel) é inválido."
        }
        
        return nil
    }
}
