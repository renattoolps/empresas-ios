//
//  CompositeValidations.swift
//  Validation
//
//  Created by Renato Lopes on 06/12/20.
//

import Foundation

/// This class is used to composite many validations
public final class ValidationComposite: ValidationProtocol {
    private let validations: [ValidationProtocol]
    
    public init(validations: [ValidationProtocol]) {
        self.validations = validations
    }
    
    public func validate(data: [String : Any]?) -> String? {
        for validation in validations {
            if let errorMessage: String = validation.validate(data: data) {
                return errorMessage
            }
        }
        
        return nil
    }
}
